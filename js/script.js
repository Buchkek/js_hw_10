function progress(storyPoints, listToDo, date) {

  let sumStoryPoints = 0;
  let sumListToDo = 0;
  for (worker of storyPoints) {
    sumStoryPoints += worker;
  }
  for (item of listToDo) {
    sumListToDo += item;
  }
  let days = Math.ceil(sumListToDo / sumStoryPoints); // number of workdays
  let workDays = days;

  let now = new Date();
  let dateForLoop = date.setDate(date.getDate() + 1);
  while (true) {
    if (now < dateForLoop) {
      if (days > 0){
        if (now.getDay() == 0 || now.getDay() == 6) {
          now.setDate(now.getDate() + 1);
          continue;
        }else{
          days--;
          now.setDate(now.getDate() + 1);
          continue;
        }
      } else {
        let daysLeft = Math.ceil((date - now) / (24*60*60*1000));
        return `Все задачи будут успешно выполнены за ${daysLeft} дней до наступления дедлайна!`
      }
    }
    else {
      let x = days*8;
      if (x == 0) {
        return `Все сделано!!!.`
      } else {
        return `Команде разработчиков придется потратить дополнительно ${x} часов после дедлайна,
        чтобы выполнить все задачи в беклоге.`
      }
    }
  }
}

let storyPoints = [9, 5, 7, 12];
let listToDo = [121,114,109,15,137];
let date = new Date("11/09/2018");
alert(progress(storyPoints, listToDo, date));
